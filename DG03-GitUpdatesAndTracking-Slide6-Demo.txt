# Ensure that in Windows you are running Git Bash
# Demo continues in the gitDiff repo

echo "some text" > index.html
git add index.html
git status
echo "some more text" >> index.html
git status
git diff
git diff --staged
git commit -m "some text added"
git status
git add index.html
git commit -m "some more text added"
git status
